using System;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;
using UnitTests.StorageTests.CloudTableMock;
using Xunit;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock.Tests
{
    public static class MockCloudTableDataStoreTests
    {
        [Fact]
        public static void MockCloudTableDataStoreThrowsOnNullEntity()
        {
            var data = new MockCloudTableDataStore();

            Assert.Throws<ArgumentNullException>(() => data.Get(null));
            Assert.Throws<ArgumentNullException>(() => data.Merge(null));
            Assert.Throws<ArgumentNullException>(() => data.Remove(null));
            Assert.Throws<ArgumentNullException>(() => data.AddOrUpdate(null));
        }
        
        [Fact]
        public static void MockCloudTableDataStoreThrowsOnNullPartitionKey()
        {
            var data = new MockCloudTableDataStore();
            
            var te = new TableEntity(null, "row");

            Assert.Throws<ArgumentException>(() => data.Get(te));
            Assert.Throws<ArgumentException>(() => data.Merge(te));
            Assert.Throws<ArgumentException>(() => data.Remove(te));
            Assert.Throws<ArgumentException>(() => data.AddOrUpdate(te));
        }
        
        [Fact]
        public static void MockCloudTableDataStoreThrowsOnNullRowKey()
        {
            var data = new MockCloudTableDataStore();
            
            var te = new TableEntity("part", null);

            Assert.Throws<ArgumentException>(() => data.Get(te));
            Assert.Throws<ArgumentException>(() => data.Merge(te));
            Assert.Throws<ArgumentException>(() => data.Remove(te));
            Assert.Throws<ArgumentException>(() => data.AddOrUpdate(te));
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanStoreAndFetch()
        {
            var data = new MockCloudTableDataStore();
            
            var te = new TableEntity
            {
                PartitionKey = "part",
                RowKey = "row"
            };

            var s1 = data.AddOrUpdate(te);
            var s2 = data.Get(te);
            
            Assert.True(te.PartitionKey.Equals(s1.PartitionKey) && te.RowKey == s1.RowKey, nameof(s1));
            Assert.True(te.PartitionKey.Equals(s2.PartitionKey) && te.RowKey == s2.RowKey, nameof(s2));
        }
        
        [Fact]
        public static void MockCloudTableDataStoreReturnsNullForNotFoundGet()
        {
            var data = new MockCloudTableDataStore();

            var te = new TableEntity("not", "found");
            
            Assert.Null(data.Get(te));
        }
        
        [Fact]
        public static void MockCloudTableDataStoreReturnsNullForNotFoundRemove()
        {
            var data = new MockCloudTableDataStore();

            var te = new TableEntity("not", "found");
            
            Assert.Null(data.Remove(te));
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanUpdate()
        {
            var data = new MockCloudTableDataStore();

            var te = new DynamicTableEntity("p1", "r1");
            te.Properties.Add("field", new EntityProperty("value"));
            data.AddOrUpdate(te);

            te.Properties.Remove("field");
            te.Properties.Add("field", new EntityProperty("updated"));

            data.AddOrUpdate(te);

            var fetched = data.Get(te) as DynamicTableEntity;
            Assert.Equal("updated", fetched.Properties["field"].StringValue);
        }

        [Fact]
        public static void MockCloudTableDataStoreCanDelete()
        {
            var data = new MockCloudTableDataStore();

            var t1 = new TableEntity("p1", "r1");
            var t2 = new TableEntity("p2", "r2");
            var t3 = new TableEntity("p3", "r3");

            data.AddOrUpdate(t1);
            data.AddOrUpdate(t2);
            data.AddOrUpdate(t3);

            data.Remove(t2);
            
            Assert.True(data.Get(t1) != null, nameof(t1));
            Assert.True(data.Get(t2) == null, nameof(t2));
            Assert.True(data.Get(t3) != null, nameof(t3));
        }

        [Fact]
        public static void MockCloudTableDataStoreCanSaveDynamicEntity()
        {
            var data = new MockCloudTableDataStore();

            var t1 = new DynamicTableEntity("p1", "r1");
            t1.Properties.Add("field", new EntityProperty("value"));

            data.AddOrUpdate(t1);
            var fetched = data.Get(t1) as DynamicTableEntity;
            
            Assert.Equal("value", fetched.Properties["field"].StringValue);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanMerge()
        {
            var data = new MockCloudTableDataStore();

            var t1 = new DynamicTableEntity("p1", "r1");
            t1.Properties.Add("f1", new EntityProperty("v1"));

            data.AddOrUpdate(t1);
            
            var t2 = new DynamicTableEntity("p1", "r1");
            t2.Properties.Add("f2", new EntityProperty("v2"));

            data.Merge(t2);
            
            var fetched = data.Get(t1) as DynamicTableEntity;
            
            Assert.True(fetched.Properties["f1"].StringValue == "v1", "f1");
            Assert.True(fetched.Properties["f2"].StringValue == "v2", "f2");
        }
        
        [Fact]
        public static void MockCloudTableDataStoreMergeSucceedsWhenNotFound()
        {
            var data = new MockCloudTableDataStore();

            var t1 = new DynamicTableEntity("p1", "r1");
            t1.Properties.Add("f1", new EntityProperty("v1"));

            data.Merge(t1);
            
            var fetched = data.Get(t1) as DynamicTableEntity;
            
            Assert.True(fetched.Properties["f1"].StringValue == "v1", "f1");
        }

        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryPartitionEquals()
        {
            var data = new MockCloudTableDataStore();

            data.AddOrUpdate(new TableEntity("part_a", "row1"));
            data.AddOrUpdate(new TableEntity("part_a", "row2"));
            data.AddOrUpdate(new TableEntity("part_b", "row3"));
            data.AddOrUpdate(new TableEntity("part_c", "row4"));
            data.AddOrUpdate(new TableEntity("part_c'quoted'", "row5's"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "part_a"));
            
            var results = data.Query(query);
            Assert.Equal(2, results.Count);
            Assert.Equal("part_a", results.ElementAt(0).PartitionKey);
            Assert.Equal("part_a", results.ElementAt(1).PartitionKey);
            Assert.NotEqual(results.ElementAt(0).RowKey, results.ElementAt(1).RowKey);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryPartitionNotEquals()
        {
            var data = new MockCloudTableDataStore();
         
            data.AddOrUpdate(new TableEntity("part_a", "row1"));
            data.AddOrUpdate(new TableEntity("part_a", "row2"));
            data.AddOrUpdate(new TableEntity("part_b", "row3"));
            data.AddOrUpdate(new TableEntity("part_c", "row4"));
            data.AddOrUpdate(new TableEntity("part_c'quoted'", "row5's"));
         
            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.NotEqual, "part_a"));
                     
            var results = data.Query(query);
            Assert.Equal(3, results.Count);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryPartitionGreaterThan()
        {
            var data = new MockCloudTableDataStore();

            data.AddOrUpdate(new TableEntity("part_a", "row1"));
            data.AddOrUpdate(new TableEntity("part_a", "row2"));
            data.AddOrUpdate(new TableEntity("part_b", "row3"));
            data.AddOrUpdate(new TableEntity("part_c", "row4"));
            data.AddOrUpdate(new TableEntity("part_c'quoted'", "row5's"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.GreaterThan, "part_c"));
            
            var results = data.Query(query);
            Assert.Single(results);
            Assert.Equal("part_c'quoted'", results.ElementAt(0).PartitionKey);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryPartitionGreaterThanOrEqual()
        {
            var data = new MockCloudTableDataStore();

            data.AddOrUpdate(new TableEntity("part_a", "row1"));
            data.AddOrUpdate(new TableEntity("part_a", "row2"));
            data.AddOrUpdate(new TableEntity("part_b", "row3"));
            data.AddOrUpdate(new TableEntity("part_c", "row4"));
            data.AddOrUpdate(new TableEntity("part_c'quoted'", "row5's"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.GreaterThanOrEqual, "part_b"));
            
            var results = data.Query(query);
            Assert.Equal(3, results.Count);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryPartitionLessThan()
        {
            var data = new MockCloudTableDataStore();

            data.AddOrUpdate(new TableEntity("part_a", "row1"));
            data.AddOrUpdate(new TableEntity("part_a", "row2"));
            data.AddOrUpdate(new TableEntity("part_b", "row3"));
            data.AddOrUpdate(new TableEntity("part_c", "row4"));
            data.AddOrUpdate(new TableEntity("part_c'quoted'", "row5's"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.LessThan, "part_c"));
            
            var results = data.Query(query);
            Assert.Equal(3, results.Count);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryPartitionLessThanOrEqual()
        {
            var data = new MockCloudTableDataStore();

            data.AddOrUpdate(new TableEntity("part_a", "row1"));
            data.AddOrUpdate(new TableEntity("part_a", "row2"));
            data.AddOrUpdate(new TableEntity("part_b", "row3"));
            data.AddOrUpdate(new TableEntity("part_c", "row4"));
            data.AddOrUpdate(new TableEntity("part_c'quoted'", "row5's"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.LessThanOrEqual, "part_c"));
            
            var results = data.Query(query);
            Assert.Equal(4, results.Count);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryRowEquals()
        {
            var data = new MockCloudTableDataStore();

            var t1 = data.AddOrUpdate(new TableEntity("part_a", "row1"));
            var t2 = data.AddOrUpdate(new TableEntity("part_a", "row2"));
            var t3 = data.AddOrUpdate(new TableEntity("part_b", "row2"));
            var t4 = data.AddOrUpdate(new TableEntity("part_c", "row3"));
            var t5 = data.AddOrUpdate(new TableEntity("part_d", "row4"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, "row2"));
            
            var results = data.Query(query);
            Assert.Equal(2, results.Count);
            Assert.Contains(t2, results);
            Assert.Contains(t3, results);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryRowNotEquals()
        {
            var data = new MockCloudTableDataStore();

            var t1 = data.AddOrUpdate(new TableEntity("part_a", "row1"));
            var t2 = data.AddOrUpdate(new TableEntity("part_a", "row2"));
            var t3 = data.AddOrUpdate(new TableEntity("part_b", "row2"));
            var t4 = data.AddOrUpdate(new TableEntity("part_c", "row3"));
            var t5 = data.AddOrUpdate(new TableEntity("part_d", "row4"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.NotEqual, "row2"));
            
            var results = data.Query(query);
            Assert.Equal(3, results.Count);
            Assert.Contains(t1, results);
            Assert.Contains(t4, results);
            Assert.Contains(t5, results);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryRowLessThan()
        {
            var data = new MockCloudTableDataStore();

            var t1 = data.AddOrUpdate(new TableEntity("part_a", "row1"));
            var t2 = data.AddOrUpdate(new TableEntity("part_a", "row2"));
            var t3 = data.AddOrUpdate(new TableEntity("part_b", "row2"));
            var t4 = data.AddOrUpdate(new TableEntity("part_c", "row3"));
            var t5 = data.AddOrUpdate(new TableEntity("part_d", "row4"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, "row2"));
            
            var results = data.Query(query);
            Assert.Single(results);
            Assert.Contains(t1, results);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryRowLessThanOrEqual()
        {
            var data = new MockCloudTableDataStore();

            var t1 = data.AddOrUpdate(new TableEntity("part_a", "row1"));
            var t2 = data.AddOrUpdate(new TableEntity("part_a", "row2"));
            var t3 = data.AddOrUpdate(new TableEntity("part_b", "row2"));
            var t4 = data.AddOrUpdate(new TableEntity("part_c", "row3"));
            var t5 = data.AddOrUpdate(new TableEntity("part_d", "row4"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, "row2"));
            
            var results = data.Query(query);
            Assert.Equal(3, results.Count);
            Assert.Contains(t1, results);
            Assert.Contains(t2, results);
            Assert.Contains(t3, results);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryRowGreaterThan()
        {
            var data = new MockCloudTableDataStore();

            var t1 = data.AddOrUpdate(new TableEntity("part_a", "row1"));
            var t2 = data.AddOrUpdate(new TableEntity("part_a", "row2"));
            var t3 = data.AddOrUpdate(new TableEntity("part_b", "row2"));
            var t4 = data.AddOrUpdate(new TableEntity("part_c", "row3"));
            var t5 = data.AddOrUpdate(new TableEntity("part_d", "row4"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThan, "row3"));
            
            var results = data.Query(query);
            Assert.Single(results);
            Assert.Contains(t5, results);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryRowGreaterThanOrEqual()
        {
            var data = new MockCloudTableDataStore();

            var t1 = data.AddOrUpdate(new TableEntity("part_a", "row1"));
            var t2 = data.AddOrUpdate(new TableEntity("part_a", "row2"));
            var t3 = data.AddOrUpdate(new TableEntity("part_b", "row2"));
            var t4 = data.AddOrUpdate(new TableEntity("part_c", "row3"));
            var t5 = data.AddOrUpdate(new TableEntity("part_d", "row4"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, "row3"));
            
            var results = data.Query(query);
            Assert.Equal(2, results.Count);
            Assert.Contains(t4, results);
            Assert.Contains(t5, results);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryPartitionAndRowExactMatch()
        {
            var data = new MockCloudTableDataStore();

            var t1 = data.AddOrUpdate(new TableEntity("part_a", "row1"));
            var t2 = data.AddOrUpdate(new TableEntity("part_a", "row2"));
            var t3 = data.AddOrUpdate(new TableEntity("part_b", "row2"));
            var t4 = data.AddOrUpdate(new TableEntity("part_c", "row3"));
            var t5 = data.AddOrUpdate(new TableEntity("part_d", "row4"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "part_a"),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, "row2")));
            
            var results = data.Query(query);
            Assert.Single(results);
            Assert.Contains(t2, results);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryPartitionAndRowGreaterThan()
        {
            var data = new MockCloudTableDataStore();

            var t1 = data.AddOrUpdate(new TableEntity("part_a", "row1"));
            var t2 = data.AddOrUpdate(new TableEntity("part_a", "row2"));
            var t3 = data.AddOrUpdate(new TableEntity("part_b", "row2"));
            var t4 = data.AddOrUpdate(new TableEntity("part_c", "row3"));
            var t5 = data.AddOrUpdate(new TableEntity("part_d", "row4"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "part_a"),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, "row1")));
            
            var results = data.Query(query);
            Assert.Equal(2, results.Count);
            Assert.Contains(t1, results);
            Assert.Contains(t2, results);
        }
        
        [Fact]
        public static void MockCloudTableDataStoreCanQueryPartitionOrRow()
        {
            var data = new MockCloudTableDataStore();

            var t1 = data.AddOrUpdate(new TableEntity("part_a", "row1"));
            var t2 = data.AddOrUpdate(new TableEntity("part_a", "row2"));
            var t3 = data.AddOrUpdate(new TableEntity("part_b", "row2"));
            var t4 = data.AddOrUpdate(new TableEntity("part_c", "row3"));
            var t5 = data.AddOrUpdate(new TableEntity("part_d", "row4"));

            var query = new TableQuery<TableEntity>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "part_a"),
                    TableOperators.Or,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, "row2")));
            
            var results = data.Query(query);
            Assert.Equal(3, results.Count);
            Assert.Contains(t1, results);
            Assert.Contains(t2, results);
            Assert.Contains(t3, results);
        }
    }
}