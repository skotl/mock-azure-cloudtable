using System;
using Microsoft.WindowsAzure.Storage.Table;
using Xunit;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock.Tests
{
    public static class MockCloudTableTests
    {
        private class SimpleModel : TableEntity
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public string Description { get; set; }    
        }
        
        private static MockCloudTable GetMockCloudTable()
        {
            return new MockCloudTable(new Uri("https://127.0.0.1:9999/account/table"));
        }

        [Fact]
        public static void MockCloudTableCanConstruct()
        {
            var cloudTable = GetMockCloudTable();
        }

        [Fact]
        public static void MockCloudTableCanInsert()
        {
            var cloudTable = GetMockCloudTable();

            var te = new TableEntity("part", "row");
            var op = TableOperation.Insert(te);

            var result = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();

            Assert.Equal(200, result.HttpStatusCode);
        }
        
        [Fact]
        public static void MockCloudTableCanInsertModel()
        {
            var model = new SimpleModel {Name = "Test", PartitionKey = "part", RowKey = "row"};
            
            var cloudTable = GetMockCloudTable();

            var op = TableOperation.Insert(model);

            var result = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();

            Assert.True(result.HttpStatusCode == 200, nameof(result.HttpStatusCode));
            Assert.True(((SimpleModel) result.Result).Name.Equals("Test"), nameof(SimpleModel.Name));
        }

        [Fact]
        public static void MockCloudTableCanFetch()
        {
            var cloudTable = GetMockCloudTable();

            var te = new TableEntity("part", "row");
            var op = TableOperation.Insert(te);

            var result = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();

            op = TableOperation.Retrieve("part", "row");
            var fetched = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();

            Assert.Equal(200, fetched.HttpStatusCode);
            Assert.Equal("row", ((ITableEntity) fetched.Result).RowKey);
        }
        
        [Fact]
        public static void MockCloudTableCanFetchModel()
        {
            var model = new SimpleModel {Name = "Test", PartitionKey = "part", RowKey = "row"};
            
            var cloudTable = GetMockCloudTable();

            var op = TableOperation.Insert(model);

            cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();
            
            op = TableOperation.Retrieve<SimpleModel>("part", "row");
            var fetched = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();

            Assert.True(fetched.HttpStatusCode == 200, nameof(fetched.HttpStatusCode));
            Assert.True(((SimpleModel) fetched.Result).Name.Equals("Test"), nameof(SimpleModel.Name));
            Assert.True(((SimpleModel) fetched.Result).Age.Equals(0), nameof(SimpleModel.Age));
        }
        
        [Fact]
        public static void MockCloudTableCanMerge()
        {
            var model = new SimpleModel {Name = "Test", PartitionKey = "part", RowKey = "row"};
            
            var cloudTable = GetMockCloudTable();

            var op = TableOperation.Insert(model);

            cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();

            DynamicTableEntity toMerge = new DynamicTableEntity("part", "row");
            toMerge.ETag = "*";
            toMerge.Properties.Add("animal", EntityProperty.GeneratePropertyForString("cat"));

            op = TableOperation.Merge(toMerge);
            cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();
            
            op = TableOperation.Retrieve<SimpleModel>("part", "row");
            var fetched = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();
            var entity = (DynamicTableEntity) fetched.Result;

            Assert.True(fetched.HttpStatusCode == 200, nameof(fetched.HttpStatusCode));
            Assert.True(entity.Properties["Name"].StringValue.Equals("Test"), "Name");
            Assert.True(entity.Properties["animal"].StringValue.Equals("cat"), "animal");
        }

        [Fact]
        public static void MockCloudTableFetchNotFoundReports404()
        {
            var cloudTable = GetMockCloudTable();

            var op = TableOperation.Retrieve("part", "row");
            var fetched = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();

            Assert.Equal(404, fetched.HttpStatusCode);
        }

        [Fact]
        public static void MockCloudTableCanDelete()
        {
            var cloudTable = GetMockCloudTable();

            var te = new TableEntity("part", "row");
            var op = TableOperation.Insert(te);

            var result = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();

            op = TableOperation.Retrieve("part", "row");
            var fetched = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();
            Assert.True(fetched.HttpStatusCode == 200, "fetch 1");

            te.ETag = "*";
            op = TableOperation.Delete(te);
            fetched = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();
            Assert.True(fetched.HttpStatusCode == 200, "delete");

            op = TableOperation.Retrieve("part", "row");
            fetched = cloudTable.ExecuteAsync(op).GetAwaiter().GetResult();
            Assert.True(fetched.HttpStatusCode == 404, "fetch 2");
        }

        [Fact]
        public static void MockCloudTableCanQuery()
        {
            var cloudTable = GetMockCloudTable();
            
            var te1 = new TableEntity("Smith", "Edwards");
            var te2 = new TableEntity("Smith", "Duane");
            var te3 = new TableEntity("Smith", "Albert");

            cloudTable.ExecuteAsync(TableOperation.Insert(te1)).GetAwaiter().GetResult();
            cloudTable.ExecuteAsync(TableOperation.Insert(te2)).GetAwaiter().GetResult();
            cloudTable.ExecuteAsync(TableOperation.Insert(te3)).GetAwaiter().GetResult();
            
            var query = new TableQuery<TableEntity>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "Smith"),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, "E")));

            TableContinuationToken token = null;
            var results = cloudTable.ExecuteQuerySegmentedAsync(query, token).GetAwaiter().GetResult();

            Assert.True(results.Results.Count == 2, "Results count");
            Assert.True(results.ContinuationToken == null, "Continuation token");
            Assert.True(results.Results.Contains(te2), nameof(te2));
            Assert.True(results.Results.Contains(te3), nameof(te3));
        }
    }
}