using System.Reflection;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock
{
    /// <summary>
    /// Instantiate non-public constructors
    /// <para>This class isn't intended to be used in production - it's purpose is as part of a mock</para>
    /// </summary>
    internal static class ClassInstantiator
    {
        /// <summary>
        /// Attempt to create an instance of a class with a non-public constructor
        /// </summary>
        /// <param name="args"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T CreateInstance<T>(params object[] args)
        {
            var type = typeof (T);
            var instance = type.Assembly.CreateInstance(
                type.FullName, false,
                BindingFlags.Instance | BindingFlags.NonPublic,
                null, args, null, null);
            return (T) instance;
        }
    }
}