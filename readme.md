# MockCloudTable

This project presents a mock [Azure CloudTable](https://docs.microsoft.com/en-us/dotnet/api/microsoft.azure.cosmos.table.cloudtable?view=azure-dotnet) that can be used in unit tests.

## Building
The easiest way (until I create a nuget package) is to use `git submodule` to include the code within your test project.

## Using
You can instantiate a `MockCloudTable` in a similar way to how you'd create a `CloudTable`:

```c#
var table = new MockCloudTable(new Uri("https://127.0.0.1:9999/account/countries"));
```
 
 Note that you need to supply a URI as this is verified by the underlying `CloudTable` base class. 
 That said, it's never used after construction so although it needs to have a valid structure the _actual_ construct doesn't matter.
 
 In the above example I typically just set the table name to something distinct for each case. For example:
 
 ```c#
var countryTable = new MockCloudTable(new Uri("https://127.0.0.1:9999/account/countries"));
var userTable = new MockCloudTable(new Uri("https://127.0.0.1:9999/account/users"));
var priceTable = new MockCloudTable(new Uri("https://127.0.0.1:9999/account/prices"));
 ```

Once the mock table has been instantiated it can be accessed like a `CloudTable` for _almost_ all operations. See the [limitations](#limitations) section, below.

## Accessing the underlying data store
The mock table stores its data in a `MockCloudTableDataStore` which can be queried directly from your unit tests, and also updated if you need to set up data outside of the `MockCloudTable` class.
Generally, there is little benefit in interacting directly with the data store methods for writing to the data store (just use the `MockCloudTable` methods instead), however the `AllRows` property can be extremely useful to query the values that have been written.
 
Supported properties and methods are:

`AllRows` property.

This property allows (read-only) access to the `ITableEntity` list within the data store.
See the [Querying the data store](#Querying_the_data_store) section, below.

`public ITableEntity AddOrUpdate(ITableEntity te)`

Add a table entity to the data store. Like the cloud table, the `ITableEntity.PartitionKey` and `ITableEntity.RowKey` properties determine whether this is inserting or replacing an item.

`public ITableEntity Remove(ITableEntity te)`

Removes an entity from the data store. If the entity is not found then the method will return `null` (and won't throw an exception).

`public ITableEntity Get(ITableEntity te)`

`public ITableEntity Get(string partitionKey, string rowKey)`

Get an entity from the data store, or return `null` if not found.

`public ITableEntity Merge(ITableEntity te)`

Merge a table entity with an entity already in the data store. Note that this converts the underlying entity to a `DynamicTableEntity`.

`public List<T> Query<T>(TableQuery<T> query) where T : ITableEntity, new()`
Apply a `TableQuery` to the data store. Note the [limitations](#limitations) section, below

## Querying the data store

It can be useful to query the underlying data store's `AllRows` property to find out what data has been written to the mock table.
Here is an example of querying for a specific type of entity and then performing tests against it:

```c#
var entities = table.AllRows.OfType<PriceEntity>().ToList();

Assert.Equal(2, entities.Count());
Assert.Contains(priceId1, entities.Select(e => e.PriceId));
Assert.Contains(priceId2, entities.Select(e => e.PriceId));
```


## Limitations

The `MockCloudTable` supports a subset of `CloudTable` functionality, including:

```c#
public override async Task<TableResult> ExecuteAsync(TableOperation operation)
```

Supported values for `TableOperation` are:
1. Delete
1. Insert (performs InsertOrReplace)
1. Replace (performs InsertOrReplace)
1. InsertOrReplace
1. Retrieve
1. Merge (performs InsertOrMerge)
1. InsertOrMerge
1. RotateEncryptionKey (no effect)

```c#
Task<TableQuerySegment<T>> ExecuteQuerySegmentedAsync<T>(
            TableQuery<T> query,
            TableContinuationToken token)
```

The query operation only supports querying of `PartitionKey` and `RowKey`.


