using System;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock.QueryBuilder
{
    internal class ExpandedFilterSet
    {
        public ExpandedFilter Clause1 { get; set; }
        public JoinOperands Join { get; set; }
        public ExpandedFilter Clause2 { get; set; }

        public ExpandedFilterSet(RawFilter filter)
        {
            if (filter == null)
                throw new ArgumentNullException(nameof(filter));
            if (string.IsNullOrWhiteSpace(filter.Clause1))
                throw new ArgumentException(nameof(filter.Clause1));

            Clause1 = new ExpandedFilter(filter.Clause1);
            Join = filter.Join;
            
            if (!string.IsNullOrWhiteSpace(filter.Clause2))
                Clause2 = new ExpandedFilter(filter.Clause2);
        }

        public override string ToString()
        {
            return $"({Clause1}) {Join} ({Clause2})";
        }
    }
}