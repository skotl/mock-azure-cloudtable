using System;
using AmazonWishList.Storage.DataAccess;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock.QueryBuilder
{
    internal class ExpandedFilter
    {
        public enum PropertyName
        {
            PartitionKey,
            RowKey
        };

        public PropertyName Property { get; set; }
        public TableQueryComparisons.Comparisons Comparison { get; set; }
        public string Value { get; set; }

        public ExpandedFilter(string filter)
        {
            var args = filter.Split(new char[] {' '}, 3, StringSplitOptions.RemoveEmptyEntries);
            if (args.Length != 3)
                throw new MockCloudDataStoryQueryParseException("Invalid format", filter);

            Property = GetProperty(args[0]);
            Comparison = TableQueryComparisons.TableQueryMap(args[1]);
            Value = TrimQuotes(args[2]);
        }

        private ExpandedFilter.PropertyName GetProperty(string prop)
        {
            switch (prop.ToLower())
            {
                case "partitionkey":
                    return ExpandedFilter.PropertyName.PartitionKey;
                case "rowkey":
                    return ExpandedFilter.PropertyName.RowKey;
                default: throw new MockCloudDataStoryQueryParseException($"Invalid property: {prop}");
            }
        }

        private string TrimQuotes(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if ((value.StartsWith('\'') && value.EndsWith('\'')) ||
                (value.StartsWith('"') && value.EndsWith('"')))
            {
                return value.Length == 2 ? "" : value.Substring(1, value.Length - 2);
            }

            return value;
        }

        public override string ToString()
        {
            return $"{Property} {Comparison} {Value}";
        }
    }
}