using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock.QueryBuilder
{
    internal class RawFilter
    {
        public string Clause1 { get; set; }
        public JoinOperands Join { get; set; } = JoinOperands.None;
        public string Clause2 { get; set; }
        

        public RawFilter(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                throw new ArgumentException(nameof(query));
            
            var queryParts = BreakupQuery(query);

            if (queryParts.Count != 1 && queryParts.Count != 3)
                throw new MockCloudDataStoryQueryParseException("Invalid number of parts", query);

            Clause1 = queryParts[0];
            if (queryParts.Count == 3)
            {
                Join = ParseJoin(queryParts[1], query);
                Clause2 = queryParts[2];
            }
        }
        
        /// <summary>
        /// Given a query of the type "(x == 3) and (y > 'a')" return the component parts
        /// This is a rough and ready parser and doesn't do any significant validation
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private List<string> BreakupQuery(string query)
        {
            var parts = query.Trim().Split(new char[] {'(', ')'}, StringSplitOptions.RemoveEmptyEntries).ToList();
            if (parts.Count == 0)
                parts.Add(query);

            var trimmed = new List<string>();
            parts.ForEach(s => trimmed.Add(s.Trim()));

            return trimmed;
        }
        
        private JoinOperands ParseJoin(string join, string _query)
        {
            join = join.ToLower();
            if (string.Compare(join, JoinOperands.Or.ToString(), StringComparison.InvariantCultureIgnoreCase) == 0)
                return JoinOperands.Or;
            if (string.Compare(join, JoinOperands.And.ToString(), StringComparison.InvariantCultureIgnoreCase) == 0)
                return JoinOperands.And;

            throw new MockCloudDataStoryQueryParseException("Unable to parse join", _query);
        }
    }
}