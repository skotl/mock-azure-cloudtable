using System;
using System.Collections.Concurrent;
using Microsoft.WindowsAzure.Storage.Table;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock.QueryBuilder
{
    internal class MockCloudDataStoreQuery
    {
        
        private readonly ConcurrentDictionary<string, ITableEntity> _data;

        public MockCloudDataStoreQuery(ConcurrentDictionary<string, ITableEntity> data)
        {
            _data = data ?? throw new ArgumentNullException(nameof(data));
        }

        public ExpandedFilterSet Compose<T>(TableQuery<T> query)
            where T : ITableEntity, new()
        {
            if (query == null)
                throw new ArgumentNullException(nameof(query));
            if (query.SelectColumns != null)
                throw new NotImplementedException($"The mocker doesn't support {nameof(query.SelectColumns)}");
            if (string.IsNullOrWhiteSpace(query.FilterString))
                throw new ArgumentException(nameof(query.FilterString));

            var rawFilter = new RawFilter(query.FilterString);
            return new ExpandedFilterSet(rawFilter);
        }

       







  
    }
}