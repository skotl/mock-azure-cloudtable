using System;
using System.Text;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock.QueryBuilder
{
    internal class MockCloudDataStoryQueryParseException : Exception
    {
        public MockCloudDataStoryQueryParseException()
            : base(GetUsage())
        {}
        
        public MockCloudDataStoryQueryParseException(string msg)
            : base (GetUsage(msg))
        {}
        
        public MockCloudDataStoryQueryParseException(string msg, string filter)
            : base (GetUsage(msg, filter))
        {}
        
        public MockCloudDataStoryQueryParseException(string msg, Exception innerException)
            : base (GetUsage(msg), innerException)
        {}
        
        
        private static string GetUsage(string msg = null, string filter = null)
        {
            var usage = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(msg))
                usage.AppendLine(msg);
            
            usage.AppendLine("This mock query only supports filters in one of the following formats");
            usage.AppendLine("`property op value`");
            usage.AppendLine("`(property op value) and|or (property op value)`");
            usage.AppendLine("where 'property' must be one of PartitionKey or RowKey (no other properties are supported) and");
            usage.AppendLine("'op' is one of the literals from Microsoft.WindowsAzure.AzureStorageRepository.Table.QueryComparison");
            
            if (!string.IsNullOrWhiteSpace(filter))
                usage.AppendLine($"\r\nThe original filter was: {filter}");
            
            return usage.ToString();
        }
    }
}