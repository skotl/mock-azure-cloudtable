using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock
{
    /// <summary>
    /// Helpers to access non-public properties
    /// <para>This class isn't intended to be used in production - it's purpose is as part of a mock</para>
    /// </summary>
    internal static class PropertyAccessors
    {
        /// <summary>
        /// Get a property's value, even if it is private or internal
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        internal static object GetPropertyValue(this object obj, string propertyName)
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            var propInfo = obj.GetType().GetProperty(propertyName);

            if (propInfo == null)
                propInfo = obj.GetType().GetProperty(propertyName, BindingFlags.NonPublic | BindingFlags.Instance);
            
            if (propInfo == null)
                throw new ArgumentOutOfRangeException(
                    $"Property '{propertyName}' not found on '{obj.GetType().FullName}");

            return propInfo.GetValue(obj);
        }

        /// <summary>
        /// Get a list of properties for an object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal static IEnumerable<PropertyInfo> GetProperties(this object obj)
        {
            return obj.GetType().GetProperties().Where(p => p.MemberType == MemberTypes.Property);
        }
    }
}