using System;
using Microsoft.WindowsAzure.Storage.Table;
using UnitTests.StorageTests.CloudTableMock;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock
{
    /// <summary>
    /// Converter helpers for <see cref="DynamicTableEntity"/>
    /// <para>This class isn't intended to be used in production - it's purpose is as part of a mock</para>
    /// </summary>
    internal static class DynamicTableEntityConverter
    {
        /// <summary>
        /// Convert a <see cref="ITableEntity"/> to a <see cref="DynamicTableEntity"/>, copying its properties
        /// <para>This method isn't intended to be used in production - it's purpose is as part of a mock</para>
        /// </summary>
        /// <param name="te"></param>
        /// <returns></returns>
        internal static DynamicTableEntity Convert(ITableEntity te)
        {
            if (te is DynamicTableEntity)
                return (DynamicTableEntity) te;

            var converted = new DynamicTableEntity(te.PartitionKey, te.RowKey);
            foreach (var property in te.GetProperties())
            {
                if (property.CanRead)
                    converted.Properties.Add(property.Name, Convert(property.GetValue(te)));
            }

            return converted;
        }

        /// <summary>
        /// Given an object, convert it for use as an <see cref="EntityProperty"/>
        /// <para>This method isn't intended to be used in production - it's purpose is as part of a mock</para>
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private static EntityProperty Convert(object o)
        {
            switch (o)
            {
                case int input:
                    return EntityProperty.GeneratePropertyForInt(input);
                case bool input:
                    return EntityProperty.GeneratePropertyForBool(input);
                case Guid input:
                    return EntityProperty.GeneratePropertyForGuid(input);
                case long input:
                    return EntityProperty.GeneratePropertyForLong(input);
                case double input:
                    return EntityProperty.GeneratePropertyForDouble(input);
                case string input:
                    return EntityProperty.GeneratePropertyForString(input);
                case byte[] input:
                    return EntityProperty.GeneratePropertyForByteArray(input);
                case DateTime input:
                    return EntityProperty.GeneratePropertyForDateTimeOffset(input);
                default:
                    return EntityProperty.CreateEntityPropertyFromObject(o);
            }
        }

    }
}