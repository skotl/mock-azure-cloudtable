using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using AmazonWishList.Storage;
using AmazonWishList.Storage.DataAccess;
using Microsoft.WindowsAzure.Storage.Table;
using UnitTests.AzureStorageRepositoryTests.CloudTableMock;
using UnitTests.AzureStorageRepositoryTests.CloudTableMock.QueryBuilder;

namespace UnitTests.StorageTests.CloudTableMock
{
    /// <summary>
    /// An in-memory data store that can be used by a <see cref="MockCloudTable"/>
    /// </summary>
    internal class MockCloudTableDataStore
    {
        private readonly ConcurrentDictionary<string, ITableEntity> _data =
            new ConcurrentDictionary<string, ITableEntity>();

        /// <summary>
        /// All rows in the data store
        /// </summary>
        public ICollection<ITableEntity> AllRows => _data.Values;
        
        /// <summary>
        /// Convert a partition key and row key into a key that can be used in the local store
        /// </summary>
        /// <param name="te"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        private static string ComposeKey(ITableEntity te)
        {
            if (te == null)
                throw new ArgumentNullException(nameof(te));
            if (string.IsNullOrWhiteSpace(te.PartitionKey))
                throw new ArgumentException(nameof(te.PartitionKey));
            if (string.IsNullOrWhiteSpace(te.RowKey))
                throw new ArgumentException(nameof(te.RowKey));

            return ComposeKey(te.PartitionKey, te.RowKey);
        }

        /// <summary>
        /// Convert a partition key and row key into a key that can be used in the local store
        /// </summary>
        /// <param name="partitionKey"></param>
        /// <param name="rowKey"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        private static string ComposeKey(string partitionKey, string rowKey)
        {
            if (string.IsNullOrWhiteSpace(partitionKey))
                throw new ArgumentException(nameof(partitionKey));
            if (string.IsNullOrWhiteSpace(rowKey))
                throw new ArgumentException(nameof(rowKey));

            return string.Concat(partitionKey, ":::", rowKey);
        }

        /// <summary>
        /// Remove the specified entity from the store
        /// </summary>
        /// <param name="te"></param>
        /// <returns>The removed entity, or null if not found</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public ITableEntity Remove(ITableEntity te)
        {
            if (te == null)
                throw new ArgumentNullException(nameof(te));

            var key = ComposeKey(te);
            if (!_data.ContainsKey(key))
                return null;

            _data.Remove(key, out var removed);

            return removed;
        }

        /// <summary>
        /// Get the requested entity from the store
        /// </summary>
        /// <param name="te"></param>
        /// <returns>The requested entity, or null if not found</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public ITableEntity Get(ITableEntity te)
        {
            if (te == null)
                throw new ArgumentNullException(nameof(te));

            return Get(te.PartitionKey, te.RowKey);
        }

        /// <summary>
        /// Get the requested entity from the store
        /// </summary>
        /// <param name="partitionKey"></param>
        /// <param name="rowKey"></param>
        /// <returns>The requested entity, or null if not found</returns>
        public ITableEntity Get(string partitionKey, string rowKey)
        {
            var key = ComposeKey(partitionKey, rowKey);

            return _data.ContainsKey(key) ? _data[key] : null;
        }

        /// <summary>
        /// Add a new entity to the store, or replace it if it already exists
        /// </summary>
        /// <param name="te"></param>
        /// <returns>The written entity</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public ITableEntity AddOrUpdate(ITableEntity te)
        {
            if (te == null)
                throw new ArgumentNullException(nameof(te));

            var key = ComposeKey(te);
            if (_data.ContainsKey(key))
                _data[key] = te;
            else
                _data.AddOrUpdate(key, te, (existingKey, existingVal) => existingVal);

            return te;
        }

        /// <summary>
        /// Merge the supplied entity with one already in the store (or writes a new record if it does not exist).
        /// <para>Warning: This method saves the entity as a <see cref="DynamicTableEntity"/>, no matter
        /// what the passed type is</para>
        /// </summary>
        /// <param name="te"></param>
        /// <returns>The merged entity</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public ITableEntity Merge(ITableEntity te)
        {
            if (te == null)
                throw new ArgumentNullException(nameof(te));

            var key = ComposeKey(te);
            DynamicTableEntity existing;

            if (_data.ContainsKey(key))
                existing = DynamicTableEntityConverter.Convert(_data[key]);
            else
                existing = new DynamicTableEntity();

            var toMerge = DynamicTableEntityConverter.Convert(te);
            foreach (var prop in existing?.Properties)
            {
                if (!toMerge.Properties.ContainsKey(prop.Key))
                    toMerge.Properties.Add(prop);
            }

            _data[key] = toMerge;
            return toMerge;
        }

        /// <summary>
        /// Execute a simple query against the data store
        /// </summary>
        /// <param name="query"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> Query<T>(TableQuery<T> query) where T : ITableEntity, new()
        {
            var queryGenerator = new MockCloudDataStoreQuery(_data);

            var queryDefinition = queryGenerator.Compose(query);

            var data = _data.AsQueryable();
            var results = ApplyFilter(data, queryDefinition.Clause1);
            switch (queryDefinition.Join)
            {
                case JoinOperands.Or:
                    results = results.Union(ApplyFilter(data, queryDefinition.Clause2));
                    break;
                case JoinOperands.And:
                    results = results.Where(r => ApplyFilter(data, queryDefinition.Clause2).Contains(r));
                    break;
            }

            return results.Select(r => (T)r.Value).ToList();
        }

        /// <summary>
        /// Apply a filter to the supplied data set, which may be a single clause, or it may have a second
        /// clause with an and/or joiner
        /// </summary>
        /// <param name="results"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="NotImplementedException"></exception>
        private IQueryable<KeyValuePair<string, ITableEntity>>
            ApplyFilter(IQueryable<KeyValuePair<string, ITableEntity>> results, ExpandedFilter clause)
        {
            if (results == null)
                throw new ArgumentNullException(nameof(results));
            if (clause == null)
                throw new ArgumentNullException(nameof(clause));

            switch (clause.Property)
            {
                case ExpandedFilter.PropertyName.PartitionKey:
                    return ApplyPartitionKeyFilter(results, clause.Comparison, clause.Value);
                case ExpandedFilter.PropertyName.RowKey:
                    return ApplyRowKeyFilter(results, clause.Comparison, clause.Value);

                default: throw new NotImplementedException(clause.Property.ToString());
            }
        }

        /// <summary>
        /// Apply the supplied filter to the Partition Key
        /// </summary>
        /// <param name="results"></param>
        /// <param name="comparison"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private IQueryable<KeyValuePair<string, ITableEntity>> ApplyPartitionKeyFilter(
            IQueryable<KeyValuePair<string, ITableEntity>> results, TableQueryComparisons.Comparisons comparison,
            string value)
        {
            switch (comparison)
            {
                case TableQueryComparisons.Comparisons.Equal:
                    return results.Where(r =>
                        String.Compare(r.Value.PartitionKey, value, StringComparison.Ordinal) == 0);
                case TableQueryComparisons.Comparisons.LessThan:
                    return results.Where(r =>
                        String.Compare(r.Value.PartitionKey, value, StringComparison.Ordinal) < 0);
                case TableQueryComparisons.Comparisons.LessThanOrEqual:
                    return results.Where(r =>
                        String.Compare(r.Value.PartitionKey, value, StringComparison.Ordinal) <= 0);
                case TableQueryComparisons.Comparisons.GreaterThan:
                    return results.Where(r =>
                        String.Compare(r.Value.PartitionKey, value, StringComparison.Ordinal) > 0);
                case TableQueryComparisons.Comparisons.GreaterThanOrEqual:
                    return results.Where(r =>
                        String.Compare(r.Value.PartitionKey, value, StringComparison.Ordinal) >= 0);
                case TableQueryComparisons.Comparisons.NotEqual:
                    return results.Where(r =>
                        String.Compare(r.Value.PartitionKey, value, StringComparison.Ordinal) != 0);

                default: throw new NotImplementedException(comparison.ToString());
            }
        }

        /// <summary>
        /// Apply the supplied filter to the Row Key
        /// </summary>
        /// <param name="results"></param>
        /// <param name="comparison"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private IQueryable<KeyValuePair<string, ITableEntity>> ApplyRowKeyFilter(
            IQueryable<KeyValuePair<string, ITableEntity>> results, TableQueryComparisons.Comparisons comparison,
            string value)
        {
            switch (comparison)
            {
                case TableQueryComparisons.Comparisons.Equal:
                    return results.Where(r =>
                        String.Compare(r.Value.RowKey, value, StringComparison.Ordinal) == 0);
                case TableQueryComparisons.Comparisons.LessThan:
                    return results.Where(r =>
                        String.Compare(r.Value.RowKey, value, StringComparison.Ordinal) < 0);
                case TableQueryComparisons.Comparisons.LessThanOrEqual:
                    return results.Where(r =>
                        String.Compare(r.Value.RowKey, value, StringComparison.Ordinal) <= 0);
                case TableQueryComparisons.Comparisons.GreaterThan:
                    return results.Where(r =>
                        String.Compare(r.Value.RowKey, value, StringComparison.Ordinal) > 0);
                case TableQueryComparisons.Comparisons.GreaterThanOrEqual:
                    return results.Where(r =>
                        String.Compare(r.Value.RowKey, value, StringComparison.Ordinal) >= 0);
                case TableQueryComparisons.Comparisons.NotEqual:
                    return results.Where(r =>
                        String.Compare(r.Value.RowKey, value, StringComparison.Ordinal) != 0);

                default: throw new NotImplementedException(comparison.ToString());
            }
        }
    }
}