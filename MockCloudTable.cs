using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using UnitTests.StorageTests.CloudTableMock;

namespace UnitTests.AzureStorageRepositoryTests.CloudTableMock
{
    /// <summary>
    /// A mock for <see cref="CloudTable"/>
    /// </summary>
    internal class MockCloudTable : CloudTable
    {
        /// <summary>
        /// The in-memory data store
        /// </summary>
        private readonly MockCloudTableDataStore _dataStore = new MockCloudTableDataStore();

        /// <summary>
        /// All rows in the data store
        /// </summary>
        public ICollection<ITableEntity> AllRows => _dataStore.AllRows;
        
        /// <summary>
        /// Construct with a storage URI, which must be of the appropriate format
        /// (e.g. https://127.0.0.1:9999/account/table)
        /// </summary>
        /// <param name="tableAddress">Valid storage URI</param>
        public MockCloudTable(Uri tableAddress) : base(tableAddress)
        {
        }

        /// <summary>
        /// Unsupported constructor
        /// </summary>
        /// <param name="tableAddress"></param>
        /// <param name="credentials"></param>
        /// <exception cref="NotImplementedException"></exception>
        public MockCloudTable(StorageUri tableAddress, StorageCredentials credentials) : base(tableAddress, credentials)
        {
            throw new NotImplementedException("Use the '(Uri)' constructor");
        }

        /// <summary>
        /// Unsupported constructor
        /// </summary>
        /// <param name="tableAbsoluteUri"></param>
        /// <param name="credentials"></param>
        /// <exception cref="NotImplementedException"></exception>
        public MockCloudTable(Uri tableAbsoluteUri, StorageCredentials credentials) : base(tableAbsoluteUri,
            credentials)
        {
            throw new NotImplementedException("Use the '(Uri)' constructor");
        }

        /// <summary>
        /// Execute a query and return matching data
        /// </summary>
        /// <param name="query"></param>
        /// <param name="token"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public override async Task<TableQuerySegment<T>> ExecuteQuerySegmentedAsync<T>(
            TableQuery<T> query,
            TableContinuationToken token)
        {
            var segment = ClassInstantiator.CreateInstance<TableQuerySegment<T>>(new List<T>());
            
            var results = _dataStore.Query<T>(query);
            results.ForEach(r => segment.Results.Add(r));
            
            return await Task.FromResult(
                segment
            );
        }
        
        /// <summary>
        /// Execute a <see cref="TableOperation"/> against the mock table, and return its result
        /// </summary>
        /// <param name="operation"></param>
        /// <returns>200 for success, 404 for not found</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public override async Task<TableResult> ExecuteAsync(TableOperation operation)
        {
            if (operation == null)
                throw new ArgumentNullException(nameof(operation));

            var result = ApplyToDataStore(operation);

            return await Task.FromResult(new TableResult()
                {
                    Result = result,
                    Etag = "*",
                    HttpStatusCode = result == null ? 404 : 200
                }
            );
        }

        /// <summary>
        /// Apply the requested <see cref="TableOperation"/> to the mock data store by mapping operation
        /// type to method in the store.  
        /// </summary>
        /// <param name="operation"></param>
        /// <returns>Impacted record, or null if not found</returns>
        /// <exception cref="NotImplementedException"></exception>
        private ITableEntity ApplyToDataStore(TableOperation operation)
        {
            ITableEntity result;

            switch (operation.OperationType)
            {
                case TableOperationType.Delete:
                    result = _dataStore.Remove(operation.Entity);
                    break;
                case TableOperationType.Replace:
                case TableOperationType.InsertOrReplace:
                case TableOperationType.Insert:
                    result = _dataStore.AddOrUpdate(operation.Entity);
                    break;
                case TableOperationType.Retrieve:
                    result = _dataStore.Get(
                        operation.GetPropertyValue("RetrievePartitionKey").ToString(),
                        operation.GetPropertyValue("RetrieveRowKey").ToString());
                    break;
                case TableOperationType.Merge:
                case TableOperationType.InsertOrMerge:
                    result = _dataStore.Merge(operation.Entity);
                    break;
                case TableOperationType.RotateEncryptionKey:
                    result = operation.Entity;
                    break;

                default: throw new NotImplementedException(operation.OperationType.ToString());
            }

            return result;
        }
    }
}